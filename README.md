# Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!

Workers of the world, unite!

Пролетарии всех стран, соединяйтесь!

# Основная информация

Это репозиторий Веб-клиента реального виртуального мира Универсо (Universo) на Nuxt.js (Vue.js).

Смотрите список активных Обсуждений (Issues) здесь в репозитории https://gitlab.com/tehnokom/universo-retejo/-/issues

Более подробная информация в главном репозитории Универсо вот тут по ссылке https://gitlab.com/tehnokom/universo

Так же смотрите репозиторий с задачами по наполнению Универсо и сбору предложений https://gitlab.com/tehnokom/universo-esplorado

Наш дискорд-сервер по Универсо https://discord.gg/bsvdPy5

## Настройка сборки

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
