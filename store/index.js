import Vue from "vue";
import Vuex from "vuex";
import VueResource from "vue-resource";

import system from "./system";
import siriusoauth from "./siriusoauth";
import spa from "./spa";
import ui from "./ui";
import tabLogic from "./tabLogic";

Vue.use(VueResource);
Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    system,
    siriusoauth,
    spa,
    ui,
    tabLogic,
  },
  strict: process.env.NODE_ENV !== "production",
});
